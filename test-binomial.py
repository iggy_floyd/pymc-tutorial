#!/usr/bin/env python
### This file is a test of the binomial distributions 
###
### more details can be fund here
### http://healthyalgorithms.com/2008/11/26/mcmc-in-python-pymc-for-bayesian-probability/
###
### and here
### http://astrostatistics.psu.edu/su10/lectures/cast10-IntroBayes-2.pdf
 


import matplotlib.pyplot as plt
from pymc import *
import numpy as np
from fractions import Fraction


import __builtin__

###  Some useful function

#### Binomial Coefficient
def comb(N,k): # from scipy.comb(), but MODIFIED!
    if (k > N) or (N < 0) or (k < 0):
        return 0L
    N,k = map(long,(N,k))
    top = N
    val = 1L
    while (top > (N-k)):
        val *= top
        top -= 1
    n = 1L
    while (n < k+1L):
        val /= n
        n += 1
    return val


#### Numerical integration (http://rosettacode.org/wiki/Numerical_integration)
def simpson(f,x,h):
  return (f(x) + 4*f(x + h/2) + f(x+h))/6.0
 
def left_rect(f,x,h):
  return f(x)

def integrate( f, a, b, steps, meth):
   h = float(b-a)/steps
   ival = h * __builtin__.sum([meth(f, a+i*h, h) for i in range(steps)])
   return ival  

def integrate_scipy( f, a, b):
   from scipy import integrate
   return integrate.romberg(f,a,b)




### Tutorial on the FPS Statistic modeling


## Assume that we have N booking with n found frauds.
## Let's plot the Likelihood of the simple model for the findings frauds in some route



def LikelihoodFPS(n,N,x):
 ''' FPS Likelihoood : P(D|x), where D={N,n} is our observable. 
    x is our theory parameter
 '''
 return np.exp(binomial_like(n,N,x))


### http://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.stats.binom.html
def LikelihoodFPS_scipy(n,N,x):
 from scipy.stats import binom
 return binom.pmf(n, N, x) 


## plot different Likelihoods of different choices of the N and n
## As an Example, some values from FraudRiskRoute_v3 table of the FPS are used
##
##  N     Route              Frauds          Bookings
##  1      FR-FR-FR            154             36856
##  2      FR-MA-FR            141             5134
##  3      DE-US-DE            55              30599
##  4       Fake               60               100
##
x = np.arange(0.001, 1.0, 0.001);
y1 = [LikelihoodFPS(154,36856,xi) for xi in x ]
y2 = [LikelihoodFPS(141,5134,xi) for xi in x ]
y3 = [LikelihoodFPS(55,30599,xi) for xi in x ]
y4 = [LikelihoodFPS(60,100,xi) for xi in x ]
y1_scipy = [LikelihoodFPS_scipy(154,36856,xi) for xi in x ]



plt.plot(x, y1, label="P(154,36856|x)",color='red')
plt.plot(x, y2, label="P(141,5134|x)",color='blue')
plt.plot(x, y3, label="P(55,30599|x)", color='green')
plt.plot(x, y1_scipy, label="P(154,36856|x) with scipy", color='black')
plt.plot(x, y4, label="P(60,100|x) on fake data", color='magenta')
plt.xlabel('x (our prediction of the probability of frauds)')
plt.ylabel(' P(D|x)')
plt.title('Likelihood functions')


# Now add the legend with some customizations.
plt.legend(loc='upper center', shadow=True)
plt.show()



### test  the integrals of the Likelihoods. In general they are not equaled to 1
f1 = lambda x: LikelihoodFPS(154,36856,x)
f2 = lambda x: LikelihoodFPS(141,5134,xi)
f3 = lambda x: LikelihoodFPS(55,30599,xi)
f11 = lambda x: x*x


print integrate(f1,0.0,1.0,100000,simpson)
print integrate_scipy(f1,0.0,1.)
#print integrate(f2,0.0,1.0,100000,simpson)
#print integrate(f3,0.,1.0,100000,simpson)


