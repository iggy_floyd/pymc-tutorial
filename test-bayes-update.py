"""
Bayesian updating of beliefs about the bias of a coin. The prior and posterior
distributions indicate probability masses at discrete candidate values of theta.
Taken from
https://raw.githubusercontent.com/aloctavodia/Doing_bayesian_data_analysis/master/04_BayesUpdate.py
"""
import matplotlib.pyplot as plt
import numpy as np
import sys


# theta is the vector of candidate values for the parameter theta.
# n_theta_vals is the number of candidate theta values.
# The size of our model for the n_theta is 1000.
n_theta_vals = 1000.
# Now make the vector of theta values:
theta = np.linspace(1/(n_theta_vals +1), n_theta_vals /(n_theta_vals +1), n_theta_vals )

# p_theta is the vector of prior probabilities on the theta values.
p_theta = np.minimum(theta, 1-theta)  # Makes a triangular belief distribution.
p_theta = p_theta / np.sum(p_theta)     # Makes sure that beliefs sum to 1.

print "p_theta(prior)=",p_theta

## plot different Likelihoods of different choices of the N and n
## As an Example, some values from FraudRiskRoute_v3 table of the FPS are used
##
##  N     Route              Frauds          Bookings
##  1      FR-FR-FR            154             36856
##  2      FR-MA-FR            141             5134
##  3      DE-US-DE            55              30599
##  4       Fake               60               100

# Specify the data. a 'head' means a fraud booking. 'Tails' are non-fraud bookings.
data = np.repeat([1, 0], [1.54, 368.56-1.54]) # Route Nr.1 scaled by 0.01
print "data=",data
n_heads = np.sum(data)
n_tails = len(data) - n_heads

# Compute the likelihood of the data for each value of theta:
p_data_given_theta = theta**n_heads * (1-theta)**n_tails
print "p_data_given_theta=",p_data_given_theta



# Compute the posterior:
p_data = np.sum(p_data_given_theta * p_theta)
p_theta_given_data = p_data_given_theta * p_theta / p_data   # This is Bayes' rule!
print "p_theta_given_data=",p_theta_given_data 

# Plot the results.
plt.figure(figsize=(12, 11))
plt.subplots_adjust(hspace=0.7)

# Plot the prior:
plt.subplot(3, 1, 1)
plt.stem(theta, p_theta, markerfmt=' ')
plt.xlim(0, 1)
plt.xlabel('$\\theta$')
plt.ylabel('$P(\\theta)$')
plt.title('Prior')
# Plot the likelihood:
plt.subplot(3, 1, 2)
plt.stem(theta, p_data_given_theta, markerfmt=' ')
plt.xlim(0, 1)
plt.xlabel('$\\theta$')
plt.ylabel('$P(D|\\theta)$')
plt.title('Likelihood')
plt.text(0.6, np.max(p_data_given_theta)/2, 'D = %sHeads,%sTails' % (n_heads, n_tails))
# Plot the posterior:
plt.subplot(3, 1, 3)
plt.stem(theta, p_theta_given_data, markerfmt=' ')
plt.xlim(0, 1)
plt.xlabel('$\\theta$')
plt.ylabel('$P(\\theta|D)$')
plt.title('Posterior')
plt.text(0.6, np.max(p_theta_given_data)/2, 'P(D) = %g' % p_data)

plt.savefig('plots/test-bayes-update.png')
