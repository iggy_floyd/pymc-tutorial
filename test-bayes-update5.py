"""
Inferring two binomial proportions via grid aproximation.
"""
from __future__ import division
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D
from scipy.stats import beta
from HDI_of_grid import HDI_of_grid
import numpy as np
import pymc as mc
import sys

def LikelihoodFPS(n,N,x):
 ''' FPS prior Likelihood : P(D|x), where D={N,n} is our observable. 
    x is our theory parameter
 '''
 return np.exp(mc.binomial_like(n,N,x))

## As an Example for theta vector, use some values from FraudRiskRoute_v3 table of the FPS are used
##
##  N     Route              Frauds          Bookings
##  1      FR-FR-FR            154             36856
##  2      FR-MA-FR            141             5134
##  3      DE-US-DE            55              30599
##  4       Fake               60               100

## taken from FraudRisk_v3 (Regions of Frauds)
## 
##  N       Region           Frauds         Bookings
##  1       DE                  2               8
##  2       ES                  9               391



# Specify the grid on theta1,theta2 parameter space.

#n_int = 500  # arbitrary number of intervals for grid on theta.
#theta1 = np.linspace(0, 1, n_int)
#theta2 = theta1

#theta1_grid, theta2_grid = np.meshgrid(theta1, theta2)

n_theta_vals = 1000.
# Now make the vector of theta values:
theta1= np.linspace(1/(n_theta_vals +1), n_theta_vals /(n_theta_vals +1), n_theta_vals )
theta2= np.linspace(1/(n_theta_vals +1), n_theta_vals /(n_theta_vals +1), n_theta_vals )

#p_theta1 = [LikelihoodFPS(154,36856,x) for x in  theta1_grid[0]]
p_theta1 = [LikelihoodFPS(154,36856,x) for x in  theta1]
p_theta1 = p_theta1 / np.sum(p_theta1)     # Makes sure that beliefs sum to 1.

p_theta2 = [LikelihoodFPS(2,8,x) for x in  theta2]
p_theta2 = p_theta1 / np.sum(p_theta2)     # Makes sure that beliefs sum to 1.

#prior =[a*b for a,b in zip(p_theta1,p_theta2)]
#prior =np.array(p_theta1)*np.array(p_theta2)
#prior = prior / np.sum(prior)

theta1_grid, theta2_grid = np.meshgrid(theta1, theta2)

# Specify the data. a 'head' means a fraud booking. 'Tails' are non-fraud bookings.
# assume it is a fraud booking
data=[1]
print "data=",data
n_heads = np.sum(data)
n_tails = len(data) - n_heads

# Compute the likelihood of the data for each value of theta:
#p_data_given_theta = theta1**n_heads * (1-theta1)**n_tails*theta2**n_heads * (1-theta2)**n_tails
p_data_given_theta2 = theta2**n_heads * (1-theta2)**n_tails
p_data2 = np.sum(p_theta2  * p_data_given_theta2)
#p_data_given_theta = theta1_grid**n_heads * (1-theta1_grid)**n_tails*theta2_grid**n_heads * (1-theta2_grid)**n_tails
#print "p_data_given_theta=",p_data_given_theta
p_data_given_theta = theta1**n_heads * (1-theta1)**n_tails*p_data2 



# Compute posterior from point-by-point multiplication and normalizing:
#p_data = np.sum(prior * p_data_given_theta)
#posterior = (prior * p_data_given_theta) / p_data
#p_data = np.sum(p_theta1 * p_theta2  * p_data_given_theta)
p_data = np.sum(p_theta1 * p_data_given_theta)
#posterior = (p_theta1 * p_theta2  * p_data_given_theta) / p_data
posterior = (p_theta1 *  p_data_given_theta) / p_data

print p_data
#print "posterior=",posterior
sys.exit(0)


# Specify the prior probability _masses_ on the grid.
prior_name = ("Beta","Ripples","Null","Alt")[0]  # or define your own.
if prior_name == "Beta":
    a1, b1, a2, b2 = 3, 3, 3, 3
    prior1 = beta.pdf(theta1_grid, a1, b1)
    prior2 = beta.pdf(theta2_grid, a1, b1)
    prior = prior1 * prior2
    prior = prior / np.sum(prior)

if prior_name == "Ripples":
    m1, m2, k = 0, 1, 0.75 * np.pi
    prior = np.sin((k*(theta1_grid-m1))**2 + (k*(theta2_grid-m2))**2)**2
    prior = prior / np.sum(prior)

if prior_name == "Null":
    # 1's at theta1=theta2, 0's everywhere else:
    prior = np.eye(len(theta1_grid), len(theta2_grid))
    prior = prior / np.sum(prior)

if prior_name == "Alt":
#    # Uniform:
    prior = np.ones((len(theta1_grid), len(theta2_grid)))
    prior = prior / np.sum(prior)

# Specify likelihood
z1, N1, z2, N2 = 5, 7, 2, 7  # data are specified here
likelihood = theta1_grid**z1 * (1-theta1_grid)**(N1-z1) * theta2_grid**z2 * (1-theta2_grid)**(N2-z2)


# Compute posterior from point-by-point multiplication and normalizing:
p_data = np.sum(prior * likelihood)
posterior = (prior * likelihood) / p_data

# Specify the probability mass for the HDI region
credib = .95
thin = 4
color = 'skyblue'

fig = plt.figure(figsize=(12,12))

# prior
ax = fig.add_subplot(3, 2, 1, projection='3d')
ax.plot_surface(theta1_grid[::thin,::thin], theta2_grid[::thin,::thin], prior[::thin,::thin], color=color)
ax.set_xlabel(r'$\theta1$')
ax.set_ylabel(r'$\theta1$')
ax.set_zlabel(r'$p(t1,t2)$')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

plt.subplot(3, 2, 2)
plt.contour(theta1_grid, theta2_grid, prior, colors=color)
plt.xlabel(r'$\theta1$')
plt.ylabel(r'$\theta1$')

# likelihood
ax = fig.add_subplot(3, 2, 3, projection='3d')
ax.plot_surface(theta1_grid[::thin,::thin], theta2_grid[::thin,::thin], likelihood[::thin,::thin], color=color)
ax.set_xlabel(r'$\theta1$')
ax.set_ylabel(r'$\theta1$')
ax.set_zlabel(r'$p(D|t1,t2)$')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

plt.subplot(3, 2, 4)
plt.contour(theta1_grid, theta2_grid, likelihood, colors=color)
plt.xlabel(r'$\theta1$')
plt.ylabel(r'$\theta1$')
plt.plot(0, label='z1,N1,z2,N2=%s,%s,%s,%s' % (z1, N1, z2, N2), alpha=0)
plt.legend(loc='upper left')

# posterior
ax = fig.add_subplot(3, 2, 5, projection='3d')
ax.plot_surface(theta1_grid[::thin,::thin], theta2_grid[::thin,::thin],posterior[::thin,::thin], color=color)
ax.set_xlabel(r'$\theta1$')
ax.set_ylabel(r'$\theta1$')
ax.set_zlabel(r'$p(t1,t2|D)$')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

plt.subplot(3, 2, 6)
plt.contour(theta1_grid, theta2_grid, posterior, colors=color)
plt.xlabel(r'$\theta1$')
plt.ylabel(r'$\theta1$')
plt.plot(0, label='p(D) = %.3e' % p_data, alpha=0)
plt.legend(loc='upper left')

# Mark the highest posterior density region
HDI_height = HDI_of_grid(posterior)['height']
plt.contour(theta1_grid, theta2_grid, posterior, levels=[HDI_height], colors='k')

plt.tight_layout()
plt.savefig('BernTwoGrid_%s.png' % prior_name)
plt.show()
