import pymc
import fpsmodel2
import numpy as np


# Generate the data
data1 = np.array([0])  # 0 Frauds
data2 = np.array([1])  # 1 Frauds



## As an Example for theta vector, use some values from FraudRiskRoute_v3 table of the FPS are used
##
##  N     Route              Frauds          Bookings
##  1      FR-FR-FR            154             36856
##  2      FR-MA-FR            141             5134
##  3      DE-US-DE            55              30599
##  4       Fake               60               100



prior_a = 154 # Route Nr. 1
prior_b = 36856-154 # Route Nr. 1

prior_a2 = 1
prior_b2 = 6

prior_a3 = 1
prior_b3 = 6


model1=fpsmodel2.fpsmodel2(prior_a,prior_b,prior_a2,prior_b2,data1)
model2=fpsmodel2.fpsmodel2(prior_a,prior_b,prior_a2,prior_b2,data2)



print "Bayes factor=", fpsmodel2.bayes_factor(model1, model2, iter=1000,burn=250, thin=2)
print "P(data1|model)=", model1.posterior_data()
print "P(data2|model)=", model2.posterior_data()
print "P(data1|model)/P(data2|model) =  ",model1.posterior_data()/model2.posterior_data()
print "v2 P(data1|model)=", model1.posterior_data_v2()
print "v2 P(data2|model)=", model2.posterior_data_v2()
print "v2 P(data1|model)/P(data2|model) =  ",model1.posterior_data_v2()/model2.posterior_data_v2()
