import matplotlib.pyplot as plt
from pymc import *
import numpy as np


def _y(p_b,m,a):
 p_b.value =a
 return m.logp


mc = MCMC(m)
mc.sample(iter=50000,burn=10000)
plt.hist(p_b.trace())
plt.show()

p_b = Uniform('p_b', 0.0, 1.0)
e_N = Normal('e_N', 0.0, 1.0/(10000.0)**2)
e_N_b = Normal('e_N_b', 0.0, 1.0/(10000.0)**2)

@potential
def likelihood(N_b=251527, e_N_b=e_N_b, N=241945+251527, e_N=e_N, p=p_b):
    return binomial_like(N_b + e_N_b, N + e_N, p)


x = np.arange(0.1, 0.9, 0.1);
y =  [_y(p_b,m,i) for i in x ]
plt.plot(x, y)
plt.show()


mc = MCMC([p_b, N, N_b, likelihood])

