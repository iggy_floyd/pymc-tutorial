import pymc as pm
import numpy as np
from scipy.special import beta as beta_func
#from scipy.special import gamma as gamma_func
from math import gamma as gamma_func



class fpsmodel:
    def __init__(self, prior_a,prior_b,data=[0]):
 
        self.theta = pm.Beta('theta', prior_a, prior_b)  # prior
        self.data = data
        self.prior_a = prior_a
        self.prior_b = prior_b

        @pm.potential
        def data_potential(theta=self.theta,data=data):
            return pm.bernoulli_like(data, p=theta)
            # Create summary values of the data:
            #z = sum(self.data[self.data == 1])  # number of 1's in data_vec
            #N = len(self.data)   # number of flips in data_vec
            #return pm.binomial_like(z, N,theta)
        
        self.data_potential = data_potential



    def logp(self):
        logp = []
        for theta_val in self.theta.trace():
            self.theta.value = theta_val
            #logp.append(pm.Model(self).logp)            
            #logp.append(pm.bernoulli_like(self.data, p=self.theta)+pm.beta_like(self.theta,self.prior_a,self.prior_b))
            #tens = self.max_tens(self.prior_b)  if self.prior_a >self.prior_b else self.max_tens(self.prior_a)     
            #post_a = float(self.prior_a)/tens
            #post_b = float(self.prior_b)/tens            
            logp.append(pm.bernoulli_like(self.data, p=self.theta)+pm.beta_like(self.theta,self.prior_a,self.prior_b))
        return np.array(logp)


    def logp_v2(self):
        logp = []
        # n_theta_vals is the number of candidate theta values.
        # The size of our model for the n_theta is 1000.
        n_theta_vals = 1000.
        # Now make the vector of theta values:
        theta = np.linspace(1/(n_theta_vals +1), n_theta_vals /(n_theta_vals +1), n_theta_vals )
        for theta_val in theta:            
            self.theta.value = theta_val
            logp.append(pm.bernoulli_like(self.data, p=self.theta)+pm.beta_like(self.theta,self.prior_a,self.prior_b))
        return np.array(logp)



    def posterior_data(self):
     # pymc.flib.logsum suggested by Anand Patil, http://gist.github.com/179657
     tens = self.max_tens(self.prior_b)  if self.prior_a >self.prior_b else self.max_tens(self.prior_a)     
     #post_a = float(self.prior_a)/tens
     #post_b = float(self.prior_b)/tens
     
     #return np.exp(-(pm.flib.logsum(-self.logp_v2()) - np.log(len(self.logp_v2())))) 
     return np.exp(-(pm.flib.logsum(-self.logp()) - np.log(len(self.logp())))) 
     #return np.exp(-(pm.flib.logsum(-self.logp()) )) 


   #Stirling's approximation
    def factorial_approx(self,n):
       log_n_factorial = n*np.log(n) - n
       return np.exp(log_n_factorial)
    

    def max_tens(self,n):
      s = 1
      while n > 10:
        s *=  10
        n /= 10
      return s


    def factorial(self,n): 
     if n < 2: return 1
     return reduce(lambda x, y: x*y, xrange(2, int(n)+1))
     
    def posterior_data_v2(self):

     # Rename the prior shape parameters, for convenience:
     # Create summary values of the data:
     z = sum(self.data[self.data == 1])  # number of 1's in data_vec
     N = len(self.data)   # number of flips in data_vec    
     


     # Compute the evidence, p(D):
     
     #p_data = beta_func(z+self.prior_a, N-z+self.prior_b)/beta_func(self.prior_a, self.prior_b)     
     tens = self.max_tens(self.prior_b)  if self.prior_a >self.prior_b else self.max_tens(self.prior_a)     
     post_a = float(self.prior_a)/tens
     post_b = float(self.prior_b)/tens
     p_data = beta_func(z+post_a, N-z+post_b)/beta_func(post_a, post_b)          
     return p_data







def bayes_factor(m1, m2, iter=1e6, burn=25000, thin=10, verbose=0):
    """ Approximate the Bayes factor as the harmonic mean posterior liklihood::
        K = Pr[data | m2] / Pr[data | m1]
         Pr[data | m1] ~= 1/mean(1/Pr[data_1, theta_1])
         Pr[data | m2] ~= 1/mean(1/Pr[data_2, theta_2])

    to compare 2 models.  According to Wikipedia / Jefferies, the
    interpretation of K is the following::

        K < 1         -   data supports m1
        1 <= K < 3    -   data supports m2, but is "Barely worth mentioning"
        3 <= K < 10   -   data gives "Substantial" support for m2
        10 <= K < 30  -   "Strong" support for m2
        30 <= K < 100 -   "Very strong" support for m2
        K >= 100      -   "Decisive" support for m2

    Parameters
    ----------
    m1 : object containing PyMC model vars and logp method
    m2 : object containing PyMC model vars and logp method
      m1 and m2 must each include a method called 'logp', which
      calculates the posterior log probability at all MCMC samples
      stored in the stochastic traces
    iter: int, optional
      number of iterations of MCMC
    burn: int, optional
      number of initial MCMC iters to discard
    thin: int, optional
      number of MCMC iters to discard per sample
    verbose : int, optional
      amount of output to request from PyMC

    Results
    -------
    K : estimate of the bayes factor

    Notes
    -----
    This sneaky harmonic mean of liklihood values appears in Kass and
    Raftery (1995) where it is attributed to to Newton and Raftery
    (1994)
    """
    pm.MCMC(m1).sample(iter*thin+burn, burn, thin, verbose=verbose)
    logp1 = m1.logp()

    pm.MCMC(m2).sample(iter*thin+burn, burn, thin, verbose=verbose)
    logp2 = m2.logp()

    # pymc.flib.logsum suggested by Anand Patil, http://gist.github.com/179657
    K = np.exp(pm.flib.logsum(-logp1) - np.log(len(logp1))
            - (pm.flib.logsum(-logp2) - np.log(len(logp2))))

    return K