"""
Inferring a binomial proportion using PyMC.
"""
import matplotlib.pyplot as plt
import numpy as np
import pymc as pm
from plot_post import *
import sys

# Generate the data
data = np.array([1])  # 0 Frauds




## As an Example for theta vector, use some values from FraudRiskRoute_v3 table of the FPS are used
##
##  N     Route              Frauds          Bookings
##  1      FR-FR-FR            154             36856
##  2      FR-MA-FR            141             5134
##  3      DE-US-DE            55              30599
##  4       Fake               60               100



prior_a = 154 # Route Nr. 1
prior_b = 36856-154 # Route Nr. 1

#@pm.deterministic
#def LikelihoodFPS(n,N,x):
# ''' FPS prior Likelihood : P(D|x), where D={N,n} is our observable. 
#    x is our theory parameter
# '''
# #return np.exp(mc.binomial_like(n,N,x))
# return mc.binomial_like(n,N,x)




#with pm.Model() as model:
# define the prior
theta = pm.Beta('theta', prior_a, prior_b)  # prior
# define the likelihood
#y = pm.Bernoulli('y', p=theta, observed=data)
#y = pm.Bernoulli('y', p=theta, observed=data)
y = pm.Binomial('y', n=len(data), p=theta, value=data,observed=True)
S = pm.MCMC([theta, y])
# Generate a MCMC chain
#trace = pm.Model([theta, y]).sample(5000, pm.Metropolis(),  progressbar=False)  # Use Metropolis sampling
S.sample(iter=500,burn=100,verbose=0)

#    start = pm.find_MAP()  # Find starting value by optimization
#    step = pm.NUTS()  # Instantiate NUTS sampler
#    trace = pm.sample(5000, step, start=start, progressbar=False)

# create an array with the posterior sample
#theta_sample = trace['theta']
theta_sample = theta.trace()

plt.hist(theta_sample)
plt.show()
pm.Matplot.plot(S,path='plots/')





plt.subplot(1, 2, 1)
#plt.plot(theta_sample[:500], np.arange(500), marker='o')
plt.plot(theta_sample[:500], marker='o')
#plt.xlim(0, 1)
plt.ylim(0, 0.05)
plt.ylabel(r'$\theta$')
plt.xlabel('Position in Chain')

plt.subplot(1, 2, 2)
mcmc_info = plot_post(theta_sample, xlab=r'$\theta', show_mode=False)

# Posterior prediction:
# For each step in the chain, use posterior theta to flip a coin:
y_pred = np.zeros(len(theta_sample))
for i, p_head in enumerate(theta_sample):    
    y_pred[i] = np.random.choice([0, 1], p=[1 - p_head, p_head])

# Jitter the 0,1 y values for plotting purposes:
y_pred_jittered = y_pred + np.random.uniform(-.05, .05, size=len(theta_sample))


# Now plot the jittered values:
plt.figure()
plt.plot(theta_sample[:500], y_pred_jittered[:500], 'ro')
#plt.xlim(-.1, 1.1)
plt.xlim(-.05, .05)
plt.ylim(-.1, 1.1)
plt.xlabel(r'$\theta$')
plt.ylabel('y (jittered)')

mean_y = np.mean(y_pred)
mean_theta = np.mean(theta_sample)

plt.plot(mean_y, mean_theta, 'k+', markersize=15)
plt.annotate('mean(y) = %.10f\nmean($\\theta$) = %.10f' %
    (mean_y, mean_theta), xy=(mean_y, mean_theta))
plt.plot([0, 1], [0, 1], linestyle='--')

plt.savefig('plots/BernBetaPyMCPost.png')
plt.show()





print  "\n theta stats:\n",theta.stats()
print  "\n",theta.summary()
print "theta value=",theta.value

