import matplotlib.pyplot as plt
from pymc import *
import numpy as np

p_b = Uniform('p_b', 0.0, 1.0)
N_b = Binomial('N_b', n=4138349, p=p_b, value=2118982, observed=True)
m = Model([p_b, N_b])
p_b.value = 0.5
print m.logp

def _y(p_b,m,a):
 p_b.value =a
 return m.logp

x = np.arange(0.1, 0.9, 0.1);
print x
y =  [_y(p_b,m,i) for i in x ]
plt.plot(x, y)
plt.show()

mc = MCMC(m)
mc.sample(iter=50000,burn=10000)
plt.hist(p_b.trace())
plt.show()
